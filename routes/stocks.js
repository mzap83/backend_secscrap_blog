var express = require('express');
var router = express.Router();

router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

/* GET stocks listing. */
router.get('/api/stocks', function(req, res, next) {
  res.send('this is stock info');
});

module.exports = router;
