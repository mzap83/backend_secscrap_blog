var express = require('express');
var router = express.Router();
var Users = require('../data/dbConnectors')

router.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

// router.use(function location (req, res, next) {
//   console.log('Location: ', location.origin(res))
//   next()
// })

/* GET users listing. */
router.get('/api/users', async(req, res) => {
  try{
      const users = await Users.find()
      res.json(users)
  }catch(err){
    res.send('Error ' + err)
  }
});

//get single user
router.get('/api/users/:id', async(req, res) => {
  try{
      const user = await Users.findById(req.params.id)
      res.json(user)
  }catch(err){
    res.send('Error ' + err)
  }
});

//this is where you add another field to a single user
router.patch('/api/users/:id', async(req, res) => {
  try{
      const user = await Users.findById(req.params.id)
      user.location = req.body.location
      const user1 = await user.save()
      res.json(user1)
  }catch(err){
    res.send('Error ')
  }
});


//post user
router.post('/api/users', async(req, res) => {
  const user = new Users({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email
  })
  try{
    const user1 = await user.save()
    res.json(user1)
  }catch(err){
    res.send('Error ' + err)
  }
});




module.exports = router;
